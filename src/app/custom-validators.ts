import { Observable } from 'rxjs/Rx';
import { FormControl } from '@angular/forms';

export class CustomValidators {
  static invalidProjectName(control: FormControl): {[s: string]: boolean} {
    if (control.value === 'Test') {
      return { 'invalidProjectName': true };
    }
    return null;
  }

  static asyncProjectName(control: FormControl): Promise<any> | Observable<any> {
    const promise = new Promise((resolve, reject) => {
      setTimeout(() => {
        if (control.value === 'Vitor') {
          resolve({ 'invalidProjectName': true });
        } else {
          resolve(null);
        }
      }, 2000);
    });
    return promise;
  }
}
